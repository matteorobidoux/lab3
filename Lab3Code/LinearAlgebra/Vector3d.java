// Matteo Robidoux //
// 1934997 //

package LinearAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt((this.x * this.x + this.y * this.y + this.z * this.z));
    }

    public double dotProduct(Vector3d vector) {
        return (this.x * vector.getX() + this.y * vector.getY() + this.z * vector.getZ());
    }

    public Vector3d add(Vector3d vector) {
        double a = this.x + vector.getX();
        double b = this.y + vector.getY();
        double c = this.z + vector.getZ();
        Vector3d newVector = new Vector3d(a,b,c);
        return newVector;
    }
}