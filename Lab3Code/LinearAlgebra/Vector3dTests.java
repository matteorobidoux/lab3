// Matteo Robidoux //
// 1934997 //

package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

 public class Vector3dTests {
    @Test
     public void testGetX() {
        Vector3d testX = new Vector3d(2.3,3.1,4.6);
        assertEquals(2.3,testX.getX());
    }
    @Test
    public void testGetY() {
        Vector3d testY = new Vector3d(5.1,8.9,3.3);
        assertEquals(8.9,testY.getY());
    }

    @Test
    public void testGetZ() {
        Vector3d testZ = new Vector3d(1.2,6.9,2.3);
        assertEquals(2.3,testZ.getZ());
    }

    @Test
    public void testMagnitude() {
        Vector3d testMagnitude = new Vector3d(2.2,3.1,4.9);
        assertEquals(6.201,testMagnitude.magnitude(),0.001);
    }

    @Test
    public void testDotProduct() {
        Vector3d testDotProduct = new Vector3d(2.1,4.9,7.3);
        Vector3d testDotProduct2 = new Vector3d(4.2,8.1,4.9);
        assertEquals(84.28,testDotProduct.getX() * testDotProduct2.getX() + testDotProduct.getY() * testDotProduct2.getY() + testDotProduct.getZ() * testDotProduct2.getZ());
    }

    @Test
    public void testAdd() {
        Vector3d testAdd = new Vector3d(2.3,4.9,7.3);
        Vector3d testAdd2 = new Vector3d(4.1,8.1,4.9);
        assertEquals(6.3,testAdd.getX() + testAdd2.getX(),0.1);
        assertEquals(13.0,testAdd.getY() + testAdd2.getY());
        assertEquals(12.2,testAdd.getZ() + testAdd2.getZ());
    }
}
